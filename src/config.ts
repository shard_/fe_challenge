/**
 * Application Config
 */
export default {

  // Defines the days used in the calendar
  days: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
}
