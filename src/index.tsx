import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './view/App';
import registerServiceWorker from './registerServiceWorker';
import { getAppState } from './state'

// Import css styles
import 'semantic-ui-css/semantic.min.css'
import './styles/index.css';

// Initialize the source of truth state
let state = getAppState()

// Mount React
ReactDOM.render(
  <App shifts={state.shifts} roles={state.roles} employees={state.employees} />,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
