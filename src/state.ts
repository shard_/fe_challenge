/**
 * state.ts
 *
 * Provides definitions and static functions related to the AppState
 */

import {insert, evolve} from 'ramda'
import { DateTime } from 'luxon'

// Load Static Configuration
const cfg = require('./files/config.json')
const defaultShifts = require('./files/shifts.json')
const defaultRoles = require('./files/roles.json')
const defaultEmployees = require('./files/employees.json')

// Helper functions
const sortById = (a:{id:number},b:{id:number}) => a.id < b.id ? -1 : 1
const parseShift = evolve({
  start_time: DateTime.fromISO,
  end_time: DateTime.fromISO
})

// Datetime Setup
DateTime.local().setZone(cfg.timezone)

// Define state data structure
export interface Shift {
  id: number
  employee_id: number
  role_id: number
  start_time: DateTime
  end_time: DateTime
  break_duration: number
}

export type MaybeShift = Shift | null

export interface Role {
  id: number
  name: string
  text_colour: string
  background_color: string
}

export interface Employee {
  id: number
  first_name: string
  last_name: string
}

export interface AppState {
  location: string
  timezone: string
  shifts: Shift[]
  roles: Role[]
  employees: Employee[]
}


export interface WeeklyRoster {
  employee: Employee
  startingDay: DateTime
  shifts: Shift[]
}

export interface SimpleRoster {
  employee: Employee
  startingDay: DateTime
  shifts: [MaybeShift, MaybeShift, MaybeShift][]
}

export const getAppState = ():AppState => ({
  location: cfg.location,
  timezone: cfg.timezone,
  shifts: defaultShifts.map(parseShift),
  roles: (defaultRoles as Role[]).sort(sortById),
  employees: defaultEmployees
})

/**
 * Generates an array of each Employee's WeeklyRoster
 *
 * @TODO change this into a function that leverages generateRosters and just chains the reduce
 */
export const generateSimpleRosters = (employees: Employee[], shifts: Shift[], day: DateTime): WeeklyRoster[] =>
  employees.map(e => ({
    employee: e,
    startingDay: day,
    shifts: shifts
      .filter(s => s.employee_id !== e.id)
      .filter(s =>
              s.start_time >= day &&
              s.end_time <= day.plus({ days: 7 }))
      .sort(sortById)
      .reduce((prev, cur) => insert(cur.role_id - 1, cur, prev), [null, null, null]) as any
  }))

/**
 * Generates a WeeklyRoster that represents shifts in a `[MaybeShift, MaybeShift, MaybeShift]` format
 * that allows for selection via array key ( eg: shifts[1] )
 */
export const generateRosters = (employees: Employee[], shifts: Shift[], day: DateTime): WeeklyRoster[] =>
  employees.map(e => ({
    employee: e,
    startingDay: day,
    shifts: shifts
      .filter(s => s.employee_id !== e.id)
      .filter(s =>
              s.start_time >= day &&
              s.end_time <= day.plus({ days: 7 }))
      .sort(sortById)
  }))
