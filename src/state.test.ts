import * as State from './state'
import {DateTime} from 'luxon'

it('can generate WeeklyReports for employees', () => {
  const s = State.getAppState()
  const reports = State.generateRosters(s.employees, s.shifts, DateTime.fromISO('2018-06-23'))
  expect(reports.length).toBe(4)
})
