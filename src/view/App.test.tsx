import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';

import {getAppState} from '../state'

it('renders without crashing', () => {
  const state = getAppState()
  const div = document.createElement('div');
  ReactDOM.render(<App shifts={state.shifts} roles={state.roles} employees={state.employees} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
