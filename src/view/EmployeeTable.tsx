import * as React from 'react'
import {Table as T, Button} from 'semantic-ui-react'
import { Role, WeeklyRoster } from '../state'
import cfg from '../config'
import '../styles/EmployeeTable.css'

const colorMap = (color: string) =>
  color === "#f38f60" ? 'yellow' :
  color === '#f5ffa2' ? 'orange' :
  color === '#bf8ef1' ? 'purple' : 'red'

const roleMap = (id:number) =>
  id === 1 ? 'Morning' :
  id === 2 ? 'Afternoon' : 'Night'


//    SimpleRow :: Object[] -> Object[] -> vNode
// Simplifiied bar based table
const SimpleRow = (roles: Role[]) => (roster: WeeklyRoster) => (
  <T.Row key={roster.employee.id}>
    <T.Cell key="name">{roster.employee.first_name + ' ' + roster.employee.last_name }</T.Cell>
    { cfg.days.map(day => (
      <T.Cell key={day}>
        <Button.Group widths="3">
          {roles.map(r => (
            <Button key={r.name}
            compact={true} size='mini'
            basic={roster.shifts[r.id-1] === null}
            color={roster.shifts[r.id-1] === null ? 'grey' : colorMap(r.background_color)}>{r.name}</Button>
          ))}
        </Button.Group>
      </T.Cell>
    ))}
  </T.Row>)

//    DetailRow :: Object[] -> Object[] -> vNode
// The detailed shift view
const DetailRow = (roles: Role[]) => (roster: WeeklyRoster) => (
  <T.Row key={roster.employee.id}>
    <T.Cell key="name">{roster.employee.first_name + ' ' + roster.employee.last_name }</T.Cell>
    { cfg.days.map(day => (
      <T.Cell key={day}>
        { roster.shifts.length === 0
          ? (<div key="no-shifts">No Shifts!</div>)
          : roster.shifts.map(s => (
            <div key={s.id}>{roleMap(s.role_id)} | ID: {s.id} | Time: {s.start_time.toFormat('t')} - {s.end_time.toFormat('t')}</div>
          ))
        }
      </T.Cell>
    ))}
  </T.Row>)

/**
 * EmployeeTable Component
 *
 * Handles the rendering of rosters
 * @TODO Move the parsing of rosters from AppState to here to avoid uneeded parsing
 */
class EmployeeTable extends React.Component {

  props: {
    roles: Role[],
    rosters: WeeklyRoster[],
    detailedRosters: WeeklyRoster[],
    detailed: Boolean
  }

  public render() {
    return (
      <T className="roster-table">
        <T.Header>
          <T.Row>
            <T.HeaderCell key="name">Name</T.HeaderCell>
            { cfg.days.map(day => (<T.HeaderCell key={day}>{day}</T.HeaderCell>)) }
          </T.Row>
        </T.Header>
        <T.Body>
          { this.props.detailed
            ? this.props.detailedRosters.map(DetailRow(this.props.roles))
            : this.props.rosters.map(SimpleRow(this.props.roles)) }
        </T.Body>
      </T>
)
  }
}

export default EmployeeTable
