import * as React from 'react'
import '../styles/App.css'
import { Employee, Shift, Role, generateRosters, generateSimpleRosters } from '../state'
import logo from '../img/logo.svg'
import EmployeeTable from './EmployeeTable'
import {DateTime} from 'luxon'
import {Button} from 'semantic-ui-react'

/**
 * Main App View which serves as the root
 */
class App extends React.Component {

  props: {
    roles: Role[],
    shifts: Shift[],
    employees: Employee[]
  }

  state: {
    currentDay: DateTime,
    detailed: Boolean
  }

  constructor(props: any){
    super(props)
    this.state = {
      currentDay: DateTime.local().startOf('week'),
      detailed: false
    }

    this.backWeek = this.backWeek.bind(this)
    this.forwardWeek = this.forwardWeek.bind(this)
    this.toggleDetailed = this.toggleDetailed.bind(this)
  }

  backWeek(){
    this.setState((prev:any) => ({
      currentDay: prev.currentDay.minus({weeks: 1})
    }))
  }

  forwardWeek(){
    this.setState((prev:any) => ({
      currentDay: prev.currentDay.plus({weeks: 1})
    }))
  }

  toggleDetailed(){
    this.setState((prev:any) => ({
      detailed: !prev.detailed
    }))
  }

  public render() {
    return (
      <div className="App">

        <header className="App-header">
          <Button floated="right" onClick={this.toggleDetailed} primary={true}>Toggle Detailed</Button>
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">ACME Rostering</h1>
        </header>

        <Button.Group>
          <Button onClick={this.backWeek} primary={true}>Back</Button>
          <Button>Week {this.state.currentDay.toFormat('WW | cccc, D')}</Button>
          <Button onClick={this.forwardWeek} primary={true}>Forward</Button>
        </Button.Group>

        <EmployeeTable
          detailed={this.state.detailed}
          roles={this.props.roles}
          rosters={generateSimpleRosters(this.props.employees, this.props.shifts, this.state.currentDay)}
          detailedRosters={generateRosters(this.props.employees, this.props.shifts, this.state.currentDay)} />
      </div>
    );
  }
}

export default App;
